import rsa
import gmpy2
from sympy import mod_inverse
from sympy.ntheory.modular import crt
from Crypto.Util.number import getPrime, bytes_to_long, long_to_bytes, GCD, inverse

def generate_rsa_key(bits, e=3):
    while True:
        p = getPrime(bits // 2)
        q = getPrime(bits // 2)
        n = p * q
        phi = (p - 1) * (q - 1)
        if GCD(e, phi) == 1:
            break
    d = int(gmpy2.invert(e, phi))
    return (e, n), (d, n)

# Test the modified key generation function
public_key, private_key = generate_rsa_key(512)
print("Public Key:", public_key)
print("Private Key:", private_key)

def encrypt_rsa(plaintext, public_key):
    e, n = public_key
    plaintext_long = bytes_to_long(plaintext)
    ciphertext = pow(plaintext_long, e, n)
    return ciphertext

def coppersmith_attack(ciphertexts, moduli, e):
    combined_C, combined_N = crt(moduli, ciphertexts)
    M, exact = gmpy2.iroot(combined_C, e)
    if exact:
        return int(M)
    else:
        raise ValueError("Failed to recover the message.")

def main():
    # Key generation
    bits = 512 
    message = input("Enter the secret message: ").encode()
    public_keys = []
    private_keys = []
    ciphertexts = []
    moduli = []
    
    for _ in range(3):
        public_key, private_key = generate_rsa_key(bits)
        public_keys.append(public_key)
        private_keys.append(private_key)
        moduli.append(public_key[1])
        ciphertexts.append(encrypt_rsa(message, public_key))

    print("Ciphertexts:")
    for i, c in enumerate(ciphertexts):
        print(f"Ciphertext {i+1}: {c}")

# Perform Coppersmith's attack
    try:
        recovered_message_long = coppersmith_attack(ciphertexts, moduli, public_keys[0][0])
        recovered_message_bytes = long_to_bytes(recovered_message_long)
        recovered_message_bytes = recovered_message_bytes.rstrip(b'\x00')
    
        print(f"Recovered plaintext (in bytes): {recovered_message_bytes}")
        recovered_message_str = recovered_message_bytes.decode('utf-8', errors='ignore')
        print(f"Recovered plaintext: {recovered_message_str}")


    
    except ValueError as ve:
        print(ve)

main()
