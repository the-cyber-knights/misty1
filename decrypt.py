import sys
e = 65537
p = int(sys.argv[1])
q = int(sys.argv[2])
n = p * q
def gcd(a,b):
	while b:
		a,b = b , a % b
	return a

def modinv(a,m):
	m0 , x0 , x1 = m,0,1
	if m == 1:
		return 0
	while a > 1:
		q = a // m
		m ,a = a % m , m
		x0 , x1 = x1 - q * x0, x0
	return x1 + m0 if x1 < 0 else x1

with open("encryptedmsg.txt","r") as f:
	content = f.read()
	encrypted_msg = eval(content)


def decrypt(encrypted_msg,e,p,q):
	phi = (p - 1) * (q - 1)
	d = modinv(e,phi)
	dec_msg = ''
	for c in encrypted_msg:
		m = pow(c,d,n)
		dec_msg += chr(m)
	return dec_msg

print("Broken :)) ",decrypt(encrypted_msg,e,p,q))
