from Crypto.Util import number
p = number.getPrime(256)
q = number.getPrime(256)
n = p * q
print(f"n = {n}")
with open("modulo.txt","w") as f:
	f.write(str(n))
