import sys
import rsa
import rsa_imp
from sympy import randprime

# Generate a 512-bit prime number
r = randprime(2**511, 2**512)

M = ' '.join(sys.argv[1:])

N, e= rsa_imp.public_key()
d, p, q = rsa_imp.private_key()

def string_to_bytes(message: str) -> bytes:
    return message.encode('utf-8')

def bytes_to_int(message: str) -> int:
    return int.from_bytes(string_to_bytes(message), byteorder='big')

def encrypt(message: str) -> bytes:
    return pow(bytes_to_int(message), e, N)

def multiplier():
    return pow(r, e, N)

def modified_cipher(message: str) -> int:
    return (encrypt(message) * multiplier()) % N 

def decrypt(cipher: int) -> int:
    return pow(cipher,d,N)

def recover(cipher: int) -> int:
    return(decrypt(cipher) * rsa.common.inverse(r, N)) % N

def int_to_bytes(cipher_int: int) -> bytes:
    length = (cipher_int.bit_length() + 7) // 8
    return cipher_int.to_bytes(length, byteorder='big')

def bytes_to_string(cipher_int: int) -> str:
    return int_to_bytes(cipher_int).decode('utf-8')

print("Message in integer:",bytes_to_int(M))
print("Encrypted cipher:",encrypt(M))
print ("Modified cipher:",modified_cipher(M))
print("Decryption of modified ciphertext:", decrypt(modified_cipher(M)))
print("Recovered number:", recover(modified_cipher(M)))
print("Message:",bytes_to_string(recover(modified_cipher(M))))

