import rsa

def decryption(ciphertext: bytes) -> bytes: 
    with open('private.pem', 'rb') as f:
        private_key_file_data = f.read()
        private_key = rsa.PrivateKey.load_pkcs1(private_key_file_data, padding=rsa_PADDING_TYPE_PKCS1_NONE)
    return rsa.decrypt(ciphertext, private_key)

def encryption(plaintext: bytes) -> bytes:
    with open('public.pem', 'rb') as f:
        public_key_file_data = f.read()
        public_key = rsa.PublicKey.load_pkcs1(public_key_file_data, padding=rsa_PADDING_TYPE_PKCS1_NONE)
    return rsa.encrypt(plaintext, public_key)
'''
#Example usage
message = b"This is a test message for a project"

ciphertext = encryption(message)
plaintext = decryption(ciphertext)
print("Encrypted message:", ciphertext)
print("Decrypted message:", plaintext.decode('utf-8'))
'''

def public_key() -> tuple[int, int]:
    with open('public.pem', 'rb') as f:
        public_key = rsa.PublicKey.load_pkcs1(f.read())
        return public_key.n, public_key.e

def private_key() -> tuple[int, int]:
     with open('private.pem', 'rb') as f:
         public_key = rsa.PrivateKey.load_pkcs1(f.read())
         return public_key.d, public_key.p, public_key.q

