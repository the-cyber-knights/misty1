# RSA

#Attempts to crack RSA
#Members:

Mansirat Kaur
Neha
Jasleen Kaur
Herman Kaur
Pratibha
We found 5 different ways to crack RSA:

1.FACTORISATION ATTACK:

Objective: The goal of a factorization attack is to find the prime factors of the large composite number, thereby compromising the encryption system.
Techniques: Various algorithms and methods can be used in factorization attacks, including:
Trial Division: Testing potential factors by dividing the composite number by various integers.
Elliptic Curve Factorization: Uses properties of elliptic curves to find factors more efficiently than trial division.
Quadratic Sieve: A more advanced method that uses number theory to factor large numbers.
General Number Field Sieve (GNFS): The most efficient known algorithm for factoring very large numbers, often used against RSA keys.

2.COPPERSMITH'S ATTACK:

Coppersmith's attack exploits the mathematical properties of RSA to recover a small plaintext from multiple ciphertexts encrypted with the same public exponent and different moduli. This attack is particularly effective when the same small message is encrypted with several different RSA keys, leading to ciphertexts \(c_i = m^e \mod n_i\).
Here's how it works:

1. Chinese Remainder Theorem (CRT): Combine the multiple ciphertexts and their corresponding moduli into a single combined ciphertext and modulus. This gives a new equation \(C = M^e \mod N\) where \(N\) is the product of the original moduli.
2. Root Extraction: Calculate the \(e\)-th root of the combined ciphertext \(C\) modulo \(N\). If the message \(m\) is sufficiently small, this calculation will reveal the original message.

By leveraging the properties of modular arithmetic and the structure of RSA, Coppersmith's attack can efficiently recover small plaintexts encrypted under multiple keys with a shared small exponent, bypassing the security of the individual encryptions.

3.COMMON MODULUS ATTACK:
Scenario
Consider the following scenario:
Suppose that Bob want's to communicate with Alice and uses Alice's public key (n, e,) to encrypt messages with RSA. As always, Eve is eavesdropping on the messages. He sends a couple of messages successfully, but after a while due to a CPU overheating (or some other random reason) a bit has been flipped unexpectedly and the message is encrypted with a a faulty public key (n, e2).
So given that Eve has access on the two different ciphertexts of the same message M that have been encrypted with different exponent but common modulus:
Ct1 = E(n, e1)(M)
ct2 = E(n, e2)(M)
She can recover the plaintext if gcd (e1, e2) = 1 and
gcd(ctz, n)=1

4.CHOSEN CIPHERTEXT ATTACK:

In an RSA encryption system, the public key consists of (e, N) and the private key is d, where N is the product of two large prime numbers, p and q. A plaintext message M is encrypted using the public key as C = M ^ e mod N. To decrypt, the private key d is used: M =   C ^ d mod N.
In a chosen ciphertext attack, the cipher text is manipulated in such a way that the attacker can decrypt the message. This type of attack is also called a blinding attack. Let us consider this scenario:
Bob releases his public key and Eve (the attacker) intercepts a ciphertext C that she wants to decrypt.
Eve then selects a random value r such that r is coprime with N. She then computes a new ciphertext C′ = (C * r ^ e mod N) mod N. 
Eve sends C′ to Bob and asks him to decrypt it. Bob, thinking it is a legitimate request, decrypts C′ using his private key d.
The above expression simplifies to:
(C') ^ d = (C x r ^ e) ^d = (M x r) ^ed = M x r 
Eve then divides the value by r to get the original message.
This type of attack can be avoided by using larger key size, using a good padding scheme like OAEP padding and by incorporating randomized encryption.
