#Generating public and private keys and storing in file
#Generating keys only once

import rsa

(public_key, private_key) = rsa.newkeys(512)

with open('private.pem', 'wb') as fh:
    fh.write(private_key.save_pkcs1())

with open('public.pem', 'wb') as fh:
    fh.write(public_key.save_pkcs1())
