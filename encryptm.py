import math

def gcd(a, b):
    while b:
        a, b = b, a % b
    return a

def modinv(a, m):
    """using Extended Euclidean Algorithm."""
    m0, x0, x1 = m, 0, 1
    if m == 1:
        return 0
    while a > 1:
        q = a // m
        m, a = a % m, m
        x0, x1 = x1 - q * x0, x0
    return x1 + m0 if x1 < 0 else x1


def find_two_prime_factors(n):
    for i in range(2, int(n**0.5) + 1):
        if sympy.isprime(i) and n % i == 0:
            other_factor = n // i
            if sympy.isprime(other_factor):
                return i, other_factor
    return None


p = 10000000000000000000000000000000000000121
q = 100000000000000000000000000000000000000000000000151

n = p * q
phi = (p - 1) * (q - 1)    
e = 65537

def generate_keys():
    d = modinv(e, phi)
    return (e, n), (d, n)

def encrypt(message, public_key):
    e, n = public_key
    encrypted_message = []
    for char in message:
        m = ord(char)
        c = pow(m, e, n)
        encrypted_message.append(c)
    return encrypted_message

message = input("Enter your message: ")
    
public_key, private_key = generate_keys()
print(f"Public Key: {public_key}")
#print(f"Private Key: {private_key}")

    
encrypted_message = encrypt(message, public_key)
with open("encryptedmsg.txt","w") as f:
	f.write(str(encrypted_message))

